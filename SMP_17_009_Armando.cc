
// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Tools/BinnedHistogram.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {
 
  /// CMS azimuthal decorrelations in back-to-back dijet events at 13 TeV 
  class SMP_17_009 : public Analysis {
  public:
 
    SMP_17_009()
      : Analysis("SMP_17_009")
    { }

    /// Book projections and histograms 
    void init() {
      FinalState fs;
      FastJets antikt(fs, FastJets::ANTIKT, 0.4);
      addProjection(antikt, "ANTIKT");

      _h_deltaPhi_2J.addHistogram( 200.,  300., bookHisto1D(1,1,1));
      _h_deltaPhi_2J.addHistogram( 300.,  400., bookHisto1D(1,1,2));
      _h_deltaPhi_2J.addHistogram( 400.,  500., bookHisto1D(1,1,3));
      _h_deltaPhi_2J.addHistogram( 500.,  600., bookHisto1D(1,1,4));
      _h_deltaPhi_2J.addHistogram( 600.,  700., bookHisto1D(1,1,5));
      _h_deltaPhi_2J.addHistogram( 700.,  800., bookHisto1D(1,1,6));
      _h_deltaPhi_2J.addHistogram( 800., 1000., bookHisto1D(1,1,7));
      _h_deltaPhi_2J.addHistogram( 1000.,1200., bookHisto1D(1,1,8));
      _h_deltaPhi_2J.addHistogram( 1200.,4000., bookHisto1D(1,1,9));

      _h_deltaPhi_3J.addHistogram( 200.,  300., bookHisto1D(2,1,1));
      _h_deltaPhi_3J.addHistogram( 300.,  400., bookHisto1D(2,1,2));
      _h_deltaPhi_3J.addHistogram( 400.,  500., bookHisto1D(2,1,3));
      _h_deltaPhi_3J.addHistogram( 500.,  600., bookHisto1D(2,1,4));
      _h_deltaPhi_3J.addHistogram( 600.,  700., bookHisto1D(2,1,5));
      _h_deltaPhi_3J.addHistogram( 700.,  800., bookHisto1D(2,1,6));
      _h_deltaPhi_3J.addHistogram( 800., 1000., bookHisto1D(2,1,7));
      _h_deltaPhi_3J.addHistogram( 1000.,1200., bookHisto1D(2,1,8));
      _h_deltaPhi_3J.addHistogram( 1200.,4000., bookHisto1D(2,1,9));

      for (int i=0;i<9;i++) norm[i] = 0.; 
    }

    /// Per-event analysis
    void analyze(const Event& event) {
      const Jets& jets = applyProjection<JetAlg>(event, "ANTIKT").jetsByPt(Cuts::absrap < 5. && Cuts::pT > 100*GeV);
      const Jets& lowjets = applyProjection<JetAlg>(event, "ANTIKT").jetsByPt(Cuts::absrap < 2.5 && Cuts::pT > 30*GeV);
      if (jets.size() < 2) vetoEvent;
      if (jets[0].absrap() > 2.5 || jets[1].absrap() > 2.5) vetoEvent;

      const double dphi = deltaPhi(jets[0].phi(), jets[1].phi());
      _h_deltaPhi_2J.fill(jets[0].pT(), dphi, event.weight());    
      if (lowjets.size() > 2) _h_deltaPhi_3J.fill(jets[0].pT(), dphi, event.weight());

      double ptmax[10] = { 200., 300., 400., 500., 600., 700., 800., 1000., 1200., 4000. };

      for (int i = 1; i < 10; i++){
        if (jets[0].pT() > ptmax[i-1] && jets[0].pT() < ptmax[i]) norm[i-1] = norm[i-1] + event.weight();
      } 
      
    }

    /// Scale histograms
    void finalize() {
      int region_ptmax_2J = 0;
      foreach (Histo1DPtr histo_2J, _h_deltaPhi_2J.getHistograms()) {
        if (norm[region_ptmax_2J] != 0) scale(histo_2J, 1.0/norm[region_ptmax_2J]);
        region_ptmax_2J++;  
      }
      int region_ptmax_3J = 0;         
      foreach (Histo1DPtr histo_3J, _h_deltaPhi_3J.getHistograms()) {       
        if (norm[region_ptmax_3J] != 0) scale(histo_3J, 1.0/norm[region_ptmax_3J]);
        region_ptmax_3J++;
      }
    }


  private:

    BinnedHistogram<double> _h_deltaPhi_2J;
    BinnedHistogram<double> _h_deltaPhi_3J;
    double norm[9];

  };
 

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(SMP_17_009);

}
