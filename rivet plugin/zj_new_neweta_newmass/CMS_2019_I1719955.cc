// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Tools/BinnedHistogram.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/ZFinder.hh"
#include <algorithm>

namespace Rivet {
 
  /// CMS azimuthal decorrelations in back-to-back dijet events at 13 TeV 
  class CMS_2019_I1719955 : public Analysis {
  public:
    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(CMS_2019_I1719955);
 

    /// Book projections and histograms 
    void init() {
       //const FinalState fs;
       //declare(FastJets(fs, FastJets::ANTIKT, 0.4), "ANTIKT");
       Histo1DPtr dummy;

      _mode = 0;
      if ( getOption("MODE") == "multijet" ) _mode = 1;
      else if ( getOption("MODE") == "ZJet" ) _mode = 2;
      if (_mode == 1) {
	  declare(FastJets(FinalState(), FastJets::ANTIKT, 0.4), "ANTIKT");
          _h_deltaPhi_2J.add( 200.,  300., book(dummy,1,1,1));
          _h_deltaPhi_2J.add( 300.,  400., book(dummy,2,1,1));
          _h_deltaPhi_2J.add( 400.,  500., book(dummy,3,1,1));
          _h_deltaPhi_2J.add( 500.,  600., book(dummy,4,1,1));
          _h_deltaPhi_2J.add( 600.,  700., book(dummy,5,1,1));
          _h_deltaPhi_2J.add( 700.,  800., book(dummy,6,1,1));
          _h_deltaPhi_2J.add( 800., 1000., book(dummy,7,1,1));
          _h_deltaPhi_2J.add( 1000.,1200., book(dummy,8,1,1));
          _h_deltaPhi_2J.add( 1200.,4000., book(dummy,9,1,1));

          _h_deltaPhi_3J.add( 200.,  300., book(dummy,10,1,1));
          _h_deltaPhi_3J.add( 300.,  400., book(dummy,11,1,1));
          _h_deltaPhi_3J.add( 400.,  500., book(dummy,12,1,1));
          _h_deltaPhi_3J.add( 500.,  600., book(dummy,13,1,1));
          _h_deltaPhi_3J.add( 600.,  700., book(dummy,14,1,1));
          _h_deltaPhi_3J.add( 700.,  800., book(dummy,15,1,1));
          _h_deltaPhi_3J.add( 800., 1000., book(dummy,16,1,1));
          _h_deltaPhi_3J.add( 1000.,1200., book(dummy,17,1,1));
          _h_deltaPhi_3J.add( 1200.,4000., book(dummy,18,1,1));

	  for (int i=0;i<9;i++) norm[i] = 0.;

          for (size_t ix = 0; ix < 9; ++ix) {
              book(_Nevt_ptmax_bins[ix], "TMP/Nevt_ptmax_bins" + to_string(ix));
          }
      }

      if (_mode == 2) {
          FinalState fs(Cuts::abseta < 5 && Cuts::pT > 100*MeV);
          declare(fs, "FS");

	  ZFinder zfinder(fs, Cuts::abseta < 2.5 && Cuts::pT > 30*GeV, PID::MUON, 70*GeV, 110*GeV, 0.2, ZFinder::ChargedLeptons::PROMPT, ZFinder::ClusterPhotons::NODECAY,ZFinder::AddPhotons::NO, 91.2*GeV);

	  declare(zfinder, "ZFinder");
          declare(FastJets(zfinder.remainingFinalState(), FastJets::ANTIKT, 0.4), "JetsAK4_zj");
	  
          _h_deltaPhi_2J.add( 200.,  300., book(dummy,1,1,1));
          _h_deltaPhi_2J.add( 300.,  400., book(dummy,2,1,1));
          _h_deltaPhi_2J.add( 400.,  500., book(dummy,3,1,1));
          _h_deltaPhi_2J.add( 500.,  600., book(dummy,4,1,1));
          _h_deltaPhi_2J.add( 600.,  700., book(dummy,5,1,1));
          _h_deltaPhi_2J.add( 700.,  800., book(dummy,6,1,1));
          _h_deltaPhi_2J.add( 800., 1000., book(dummy,7,1,1));
          _h_deltaPhi_2J.add( 1000.,1200., book(dummy,8,1,1));
          _h_deltaPhi_2J.add( 1200.,4000., book(dummy,9,1,1));

          _h_deltaPhi_3J.add( 200.,  300., book(dummy,10,1,1));
          _h_deltaPhi_3J.add( 300.,  400., book(dummy,11,1,1));
          _h_deltaPhi_3J.add( 400.,  500., book(dummy,12,1,1));
          _h_deltaPhi_3J.add( 500.,  600., book(dummy,13,1,1));
          _h_deltaPhi_3J.add( 600.,  700., book(dummy,14,1,1));
          _h_deltaPhi_3J.add( 700.,  800., book(dummy,15,1,1));
          _h_deltaPhi_3J.add( 800., 1000., book(dummy,16,1,1));
          _h_deltaPhi_3J.add( 1000.,1200., book(dummy,17,1,1));
          _h_deltaPhi_3J.add( 1200.,4000., book(dummy,18,1,1));	       
             
          for (int i=0;i<9;i++) norm[i] = 0.; 
          
          for (size_t ix = 0; ix < 9; ++ix) {
              book(_Nevt_ptmax_bins[ix], "TMP/Nevt_ptmax_bins" + to_string(ix));
          }
     }
    }
    

    /// Per-event analysis
    void analyze(const Event& event) {
      const double weight = 1.0;
      double ptmax[10] = { 200., 300., 400., 500., 600., 700., 800., 1000., 1200., 4000. };
      if (_mode == 1) {
          const Jets& jets = applyProjection<JetAlg>(event, "ANTIKT").jetsByPt(Cuts::absrap < 5. && Cuts::pT > 100*GeV);
          const Jets& lowjets = applyProjection<JetAlg>(event, "ANTIKT").jetsByPt(Cuts::absrap < 2.5 && Cuts::pT > 30*GeV);
          if (jets.size() < 2) vetoEvent;
          if (jets[0].absrap() > 2.5 || jets[1].absrap() > 2.5) vetoEvent;

          const double dphi = 180./M_PI*deltaPhi(jets[0].phi(), jets[1].phi());
          _h_deltaPhi_2J.fill(jets[0].pT(), dphi);    
          if (lowjets.size() > 2) _h_deltaPhi_3J.fill(jets[0].pT(), dphi);

          for (int i = 1; i < 10; i++){
            if (jets[0].pT() > ptmax[i-1] && jets[0].pT() < ptmax[i]) {
        
            norm[i-1] = norm[i-1] + weight;
            _Nevt_ptmax_bins[i-1] ->fill(); 
            }
          } 
      }
    
      if (_mode == 2) {
          const ZFinder& zfinder = apply<ZFinder>(event, "ZFinder");
          if (zfinder.bosons().size() != 1) vetoEvent;
          const Particle& z = zfinder.bosons()[0];
          const Particles leptons = sortBy(zfinder.constituents(), cmpMomByPt);

          if (leptons[0].pT() < 25.0*GeV || leptons[1].pT() < 10.0*GeV || z.pT() < 100.0*GeV || z.mass() < 70.0*GeV || z.mass() > 110*GeV) vetoEvent;
	  if (leptons[0].absrapidity() > 2.4 || leptons[1].absrapidity() > 2.4) vetoEvent;
	  if (z.absrapidity() > 2.5) vetoEvent;
           
          const PseudoJets& psjetsAK4_zj = apply<FastJets>(event, "JetsAK4_zj").pseudoJetsByPt(100.0*GeV);
	  const PseudoJets& low_psjetsAK4_zj = apply<FastJets>(event, "JetsAK4_zj").pseudoJetsByPt(30.0*GeV);
	  if (psjetsAK4_zj.empty()) vetoEvent;

	  const fastjet::PseudoJet& j0 = psjetsAK4_zj[0];
 	  const FourMomentum jmom0(j0.e(), j0.px(), j0.py(), j0.pz());

	  if (psjetsAK4_zj.size() >= 2) {
	      const fastjet::PseudoJet& j1 = psjetsAK4_zj[1];
	      const FourMomentum jmom1(j1.e(), j1.px(), j1.py(), j1.pz());
	      if (z.pT()<=jmom1.pT()) vetoEvent;
	  }

          if (jmom0.absrapidity() > 2.5 || jmom0.pT() < 100.0*GeV) vetoEvent;

          if (!(abs(deltaR(leptons[0], jmom0)) > 0.5 && deltaR(leptons[1], jmom0) > 0.5)) vetoEvent;
	  //should be changed
 	  const double dphi = 180./M_PI*abs(deltaPhi(z, jmom0));
	  const double highestpT = max(z.pT(),jmom0.pT());
          _h_deltaPhi_2J.fill(highestpT, dphi, 1.0);
          
	  if (low_psjetsAK4_zj.size() >= 2) {
	      const fastjet::PseudoJet& j2 = low_psjetsAK4_zj[1];
	      const FourMomentum jmom2(j2.e(), j2.px(), j2.py(), j2.pz());
	      if(abs(deltaR(leptons[0], jmom2)) < 0.5 || abs(deltaR(leptons[1], jmom2)) < 0.5) vetoEvent;
	      _h_deltaPhi_3J.fill(highestpT, dphi, 1.0);

	  }	
          for (int i = 1; i < 10; i++){
            if (highestpT> ptmax[i-1] && highestpT < ptmax[i]) {

            norm[i-1] = norm[i-1] + weight;
            _Nevt_ptmax_bins[i-1] ->fill();
            }
          }
      }      
 
    }

    /// Scale histograms
    void finalize() {
      if (_mode == 1) {
      double norm_finalize[9];
      cout << " in finalze " << endl ;
      for (int i=0;i<9;i++) {
         // norm_finalize[i] = norm[i];
        if (_Nevt_ptmax_bins[i]->val()  != 0)   norm_finalize[i] = _Nevt_ptmax_bins[i]->val() ; 
        cout << " CMS_2019_I1719955 checking norm factors " << norm[i] << " " << norm_finalize[i] << endl;
        cout << " CMS_2019_I1719955 checking norm factors " << norm[i] << " " << norm_finalize[i] << " " << dbl(*_Nevt_ptmax_bins[i])<< endl;
      }
      int region_ptmax_2J = 0;
      for (Histo1DPtr histo_2J : _h_deltaPhi_2J.histos()) {
        if (norm_finalize[region_ptmax_2J] != 0)  scale(histo_2J, 1.0/norm_finalize[region_ptmax_2J]);
        else  scale(histo_2J, 0.);
        
        region_ptmax_2J++;  
      }
      int region_ptmax_3J = 0;         
      for (Histo1DPtr histo_3J : _h_deltaPhi_3J.histos()) {       
        if (norm_finalize[region_ptmax_3J] != 0) scale(histo_3J, 1.0/norm_finalize[region_ptmax_3J]);
        else  scale(histo_3J, 0.);

        region_ptmax_3J++;
      }
    }
    /*
    if (_mode == 2) {
        for (Histo1DPtr histo : _h_deltaPhi_2J.histos()) normalize(histo);
	for (Histo1DPtr histo : _h_deltaPhi_3J.histos()) normalize(histo); 
    }
    */
    if (_mode == 2) {
        double norm_finalize[9];
        for (int i=0;i<9;i++) {
            if (_Nevt_ptmax_bins[i]->val()  != 0)   norm_finalize[i] = _Nevt_ptmax_bins[i]->val() ;
        }
        int region_ptmax_2J = 0;
        for (Histo1DPtr histo_2J : _h_deltaPhi_2J.histos()) {
            if (norm_finalize[region_ptmax_2J] != 0)  scale(histo_2J, 1.0/norm_finalize[region_ptmax_2J]);
            else  scale(histo_2J, 0.);
            region_ptmax_2J++;
        }
        int region_ptmax_3J = 0;
        for (Histo1DPtr histo_3J : _h_deltaPhi_3J.histos()) {
            if (norm_finalize[region_ptmax_3J] != 0) scale(histo_3J, 1.0/norm_finalize[region_ptmax_3J]);
            else  scale(histo_3J, 0.);
            region_ptmax_3J++;
        }
    }

  }
  private:

    BinnedHistogram _h_deltaPhi_2J;
    BinnedHistogram _h_deltaPhi_3J;

    double norm[9];
    array<CounterPtr,9> _Nevt_ptmax_bins;

  protected:

    size_t _mode;
  };
 

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(CMS_2019_I1719955);

}
