&CASCADE_input
NrEvents = -1                               ! Nr of events to process
!NrEvents = 60000                               ! Nr of events to process
Process_Id = -1                             ! Read LHE file
MaxFactor = 5000                               ! Max scling factor for accept/reject
Hadronisation = 0                          ! Hadronisation on (=1)
! pythia6 = 0
! Hadronisation = 0                          ! Hadronisation on (=1)
SpaceShower = 1                            ! Space-like Parton Shower(original=1)
SpaceShowerOrderAlphas=2                   ! Order alphas in Space Shower
TimeShower = 1                               ! Time-like Parton Shower(original=1)
ScaleTimeShower = 4                        ! Scale choice for Time-like Shower
!ScaleFactorFinalShower = 0.25               ! scale factor for Final State Parton Shower  
PartonEvolution = 3                        ! type of parton evolution in Space-like Shower
! EnergyShareRemnant = 4                   ! energy sharing in proton remnant
!Remnant = 0                              ! =0 no remnant treatment
PartonDensity = 102200                     ! use TMDlib
!PartonDensity = 101201                     ! use TMDlib
! TMDDensityPath= './share'                ! Path to TMD density for internal files
lheInput = 'pwgevents.lhe'  ! LHE input file
!lheInput = '/Users/jung/jung/cvs/pythia8/runs/DY-fixed_x_mass_scale.lhe'
Uncertainty_TMD = 0                        ! calculate and store uncertainty TMD pdfs
lheHasOnShellPartons = 1                  ! = 0 LHE file has off-shell parton configuration
lheReweightTMD = 0                         ! Reweight with new TMD given in PartonDensity
lheScale = 2                              ! Scale defintion for TMD: =0 use scalup, =1 use shat
!                                            0: use scalup
!                                            1: use shat
!                                            2: use 1/2 Sum pt^2 of final parton/particles
!                                            3: use shat for Born and 1/2 Sum pt^2 of final parton(particle)
!                                            4: use shat for Born and max pt of most forward/baward parton(particle)
lheNBornpart = 2                           ! Nr of hard partons (particles) (Born process)
! ScaleFactorMatchingScale = 2.0              ! Scale factor for matching scale when including TMDs
lheWeightId = 0                           ! use weight Id = ...
!                                            as weight for LHE file
&End 
&PYTHIA6_input
P6_Itune = 370          ! Retune of Perugia 2011 w CTEQ6L1          (Oct 2012)
! P6_MSTJ(45) = 4         ! Nr of flavors in final state shower: g->qqbar 
P6_MSTJ(41) = 2         !  (D = 2) type of branchings allowed in shower. =1 only QCD
!P6_MSTU(101) = 0     ! fixed alpha_em
! P6_PMAS(4,1)= 2.       ! charm mass
! P6_PMAS(5,1)= 4.75      ! bottom mass
! P6_PMAS(25,1)= 125.     ! higgs mass
! P6_MSTJ(48) =   1       ! (D=0), 0=no max. angle, 1=max angle def. in PARJ(85)
! P6_PARU(112) = 0.2      ! lambda QCD
! P6_MSTU(111) =  1       ! = 0 : alpha_s is fixed, =1 first order; =2 2nd order;
! P6_MSTU(112)=   4       ! nr of flavours wrt lambda_QCD
! P6_MSTU(113)=   3       ! min nr of flavours for alphas
! P6_MSTU(114)=   5       ! max nr of flavours for alphas
&End

