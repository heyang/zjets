import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection 
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module

from PhysicsTools.NanoAODTools.postprocessing.tools import deltaR
from PhysicsTools.NanoAODTools.postprocessing.tools import deltaPhi

class exampleProducer(Module):
    def __init__(self):
        pass
    def beginJob(self):
        pass
    def endJob(self):
        pass
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.out = wrappedOutputTree
	self.out.branch("pass_gen_selection","B");
        self.out.branch("dressed_z_mass","F");
        self.out.branch("dressed_z_pt","F");
        self.out.branch("dressed_z_phi","F");
        self.out.branch("dressed_z_eta","F");
        self.out.branch("dressed_z_energy","F");
        self.out.branch("ndressedgoodmuons","I");
        self.out.branch("dressed_muon_pt","F",lenVar="ndressedgoodmuons");
        self.out.branch("dressed_muon_eta","F",lenVar="ndressedgoodmuons");
        self.out.branch("dressed_muon_phi","F",lenVar="ndressedgoodmuons");
        self.out.branch("dressed_muon_mass","F",lenVar="ndressedgoodmuons");
        self.out.branch("dressed_muon_energy","F",lenVar="ndressedgoodmuons");
	self.out.branch("gen_jet_mass","F",lenVar="ngoodgenjets");
        self.out.branch("gen_jet_pt","F",lenVar="ngoodgenjets");
        self.out.branch("gen_jet_eta","F",lenVar="ngoodgenjets");
        self.out.branch("gen_jet_phi","F",lenVar="ngoodgenjets");
        self.out.branch("gen_dphi_zjet","F",lenVar="ngoodgenjets");
        self.out.branch("gen_dphi_jet12","F",lenVar="ngoodgenjets_dphi_jet12");
  	
    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass
    def analyze(self, event):
        """process event, return True (go to next module) or False (fail, go to next event)"""
	if hasattr(event,'nGenDressedLepton'):
            dressedparts = Collection(event, "GenDressedLepton")
	
	if hasattr(event,'nGenJet'):
	    genjets = Collection(event, "GenJet")


        dressedmuons = []
        for i in range(0,len(dressedparts)):
            if abs(dressedparts[i].pdgId) == 13 :
                dressedmuons.append(dressedparts[i])

        if (len(dressedmuons)<=1):
            self.out.fillBranch("pass_gen_selection",0)
            return True

        nvetodressedmuons = 0
        for i in range(0,len(dressedmuons)):
            if abs(dressedmuons[i].eta) < 2.4 and dressedmuons[i].pt >= 10:
                nvetodressedmuons += 1
        if nvetodressedmuons > 2:
            self.out.fillBranch("pass_gen_selection",0)
            return True

        dressed_Z = ROOT.TLorentzVector()
        dressed_tight_muons = []
	
	newdressedmuons = []
        if dressedmuons[0].pt < dressedmuons[1].pt:
            newdressedmuons.append(dressedmuons[1])
            newdressedmuons.append(dressedmuons[0])
        else:
            newdressedmuons.append(dressedmuons[0])
            newdressedmuons.append(dressedmuons[1])

        for i in range(0,len(newdressedmuons)):
            if abs(newdressedmuons[i].eta) >= 2.4:
                continue
            if (newdressedmuons[i].pt) <= 25:
                continue
            for j in range(i+1,len(newdressedmuons)):
                if abs(newdressedmuons[j].eta) >= 2.4:
                    continue
                if (newdressedmuons[j].pt) <= 20:
                    continue
                if (newdressedmuons[i].pdgId + newdressedmuons[j].pdgId == 0):
                    dressed_Z = newdressedmuons[i].p4() + newdressedmuons[j].p4()
                    if (dressed_Z.M() > 76 and dressed_Z.M() < 106):
                        self.out.fillBranch("pass_gen_selection",1)
                        self.out.fillBranch("dressed_z_pt",dressed_Z.Pt())
                        self.out.fillBranch("dressed_z_mass",dressed_Z.M())
                        self.out.fillBranch("dressed_z_phi",dressed_Z.Phi())
                        self.out.fillBranch("dressed_z_eta",dressed_Z.Eta())
                        self.out.fillBranch("dressed_z_energy",dressed_Z.E())
                        dressed_tight_muons.append(newdressedmuons[i])
                        dressed_tight_muons.append(newdressedmuons[j])

	if len(dressed_tight_muons) < 2:
            self.out.fillBranch("pass_gen_selection",0)
            return True

        ndressedgoodmuons=len(dressed_tight_muons)
        dressed_goodmuons_pt = []
        dressed_goodmuons_eta = []
        dressed_goodmuons_phi = []
        dressed_goodmuons_mass = []
        dressed_goodmuons_energy = []

        dressed_goodmuons_pt.append(dressed_tight_muons[0].pt)
        dressed_goodmuons_pt.append(dressed_tight_muons[1].pt)
        dressed_goodmuons_eta.append(dressed_tight_muons[0].eta)
        dressed_goodmuons_eta.append(dressed_tight_muons[1].eta)
        dressed_goodmuons_phi.append(dressed_tight_muons[0].phi)
        dressed_goodmuons_phi.append(dressed_tight_muons[1].phi)
        dressed_goodmuons_mass.append(dressed_tight_muons[0].mass)
        dressed_goodmuons_mass.append(dressed_tight_muons[1].mass)
        dressed_goodmuons_energy.append(dressed_tight_muons[0].p4().E())
        dressed_goodmuons_energy.append(dressed_tight_muons[1].p4().E())

        self.out.fillBranch("ndressedgoodmuons",ndressedgoodmuons)
        self.out.fillBranch("dressed_muon_pt",dressed_goodmuons_pt)
        self.out.fillBranch("dressed_muon_eta",dressed_goodmuons_eta)
        self.out.fillBranch("dressed_muon_phi",dressed_goodmuons_phi)
        self.out.fillBranch("dressed_muon_mass",dressed_goodmuons_mass)
        self.out.fillBranch("dressed_muon_energy",dressed_goodmuons_energy)
	
	ngoodgenjets = 0
        goodgenjets_pt = []
        goodgenjets_mass = []
        goodgenjets_phi = []
        goodgenjets_eta = []
        goodgenjets_dphi_zjet = []
        goodgenjets_dphi_jet12 = []
	
	for i in range(0,len(genjets)):
	    for j in range(i+1,len(genjets)):
		if genjets[j].pt>genjets[i].pt:
		    tmp = genjets[i]
		    genjets[i]=genjets[j]
		    genjets[j]=tmp
	
	for i in range(0, len(genjets)-1):

            if genjets[i].pt<genjets[i+1].pt:
		print("new ", "i=",i,"genjets pt= ",genjets[i].pt,"genjets eta= ",genjets[i].eta)
	
	
	for k in range(0,len(genjets)):
            #print(4)
            if abs(genjets[k].eta) > 2.4:
                continue
            #print(5) 
            if genjets[k].pt < 30:
                continue
            
	    pass_lepton_dr_cut = True

            for i in range(0,len(dressed_tight_muons)):
                #if deltaR(muons[tight_muons[i]].eta,muons[tight_muons[i]].phi,genjets[k].eta,genjets[k].phi) < 0.4:
                if deltaR(dressed_tight_muons[i].eta,dressed_tight_muons[i].phi,genjets[k].eta,genjets[k].phi) < 0.4:
                    pass_lepton_dr_cut = False

            if not pass_lepton_dr_cut:
                continue
            ngoodgenjets += 1
            goodgenjets_pt.append(genjets[k].pt)
            goodgenjets_mass.append(genjets[k].mass)
            goodgenjets_eta.append(genjets[k].eta)
            goodgenjets_phi.append(genjets[k].phi)
            goodgenjets_dphi_zjet.append(deltaPhi(dressed_Z.Phi(),genjets[k].phi))

	if ngoodgenjets != len(goodgenjets_pt):
            print(error)

        self.out.fillBranch("gen_jet_pt",goodgenjets_pt)
        self.out.fillBranch("gen_jet_mass",goodgenjets_mass)
        self.out.fillBranch("gen_jet_eta",goodgenjets_eta)
        self.out.fillBranch("gen_jet_phi",goodgenjets_phi)
        self.out.fillBranch("gen_dphi_zjet",goodgenjets_dphi_zjet)

        if ngoodgenjets >= 2:
            goodgenjets_dphi_jet12.append(deltaPhi(goodgenjets_phi[0],goodgenjets_phi[1]))
            #print("jet0 = ",goodgenjets_phi[0], "jet1 = ", goodgenjets_phi[1], "dphi = ", deltaPhi(goodgenjets_phi[0],goodgenjets_phi[1]), "dphi1 = ",goodgenjets_dphi_jet12[0])
        ngoodgenjets_dphi_jet12 = len(goodgenjets_dphi_jet12)
        self.out.fillBranch("gen_dphi_jet12",goodgenjets_dphi_jet12)	
	
	return True
# define modules using the syntax 'name = lambda : constructor' to avoid having them loaded when not needed

genModuleConstr = lambda : exampleProducer() 
 
