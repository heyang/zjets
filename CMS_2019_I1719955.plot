# BEGIN PLOT /CMS_2019_I1719955/d.*
XLabel=$\Delta\phi_{12}$ [deg]
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{d\Delta\phi_{12}}$ [deg$^{-1}$]
FullRange=1
RatioPlotYMin=0.85
RatioPlotYMax=1.15
LegendXPos=0.085
LogY=0
RatioPlotErrorBandColor={[rgb]{0.8,0.8,0.5}}
# END PLOT
# BEGIN PLOT /CMS_2019_I1719955/d01-x01-y01
Title=CMS, 13 TeV, $\Delta\phi_{12}$ Z+1jet,$200<p_{\text{T}}^\text{max}<300$~GeV
YMin=0.031
# END PLOT
# BEGIN PLOT /CMS_2019_I1719955/d02-x01-y01  
Title=CMS, 13 TeV, $\Delta\phi_{12}$ Z+1jet,$300<p_{\text{T}}^\text{max}<400$~GeV
YMin=0.031
# END PLOT
# BEGIN PLOT /CMS_2019_I1719955/d03-x01-y01
Title=CMS, 13 TeV, $\Delta\phi_{12}$ Z+1jet,$400<p_{\text{T}}^\text{max}<500$~GeV 
YMin=0.021
# END PLOT
# BEGIN PLOT /CMS_2019_I1719955/d04-x01-y01
Title=CMS, 13 TeV, $\Delta\phi_{12}$ Z+1jet,$500<p_{\text{T}}^\text{max}<600$~GeV
YMin=0.021
# END PLOT
# BEGIN PLOT /CMS_2019_I1719955/d05-x01-y01
Title=CMS, 13 TeV, $\Delta\phi_{12}$ Z+1jet,$600<p_{\text{T}}^\text{max}<700$~GeV
YMin=0.021
# END PLOT
# BEGIN PLOT /CMS_2019_I1719955/d06-x01-y01
Title=CMS, 13 TeV, $\Delta\phi_{12}$ Z+1jet,$700<p_{\text{T}}^\text{max}<800$~GeV
YMin=0.021
# END PLOT
# BEGIN PLOT /CMS_2019_I1719955/d07-x01-y01
Title=CMS, 13 TeV, $\Delta\phi_{12}$ Z+1jet,$800<p_{\text{T}}^\text{max}<1000$~GeV
YMin=0.011
# END PLOT
# BEGIN PLOT /CMS_2019_I1719955/d08-x01-y01
Title=CMS, 13 TeV, $\Delta\phi_{12}$ Z+1jet,$1000<p_{\text{T}}^\text{max}<1200$~GeV
YMin=0.011
# END PLOT
# BEGIN PLOT /CMS_2019_I1719955/d09-x01-y01
Title=CMS, 13 TeV, $\Delta\phi_{12}$ Z+1jet,$p_{\text{T}}^\text{max}>1200$~GeV
YMin=0.011
# END PLOT
# BEGIN PLOT /CMS_2019_I1719955/d10-x01-y01
Title=CMS, 13 TeV, $\Delta\phi_{12}$ Z+2jet,$200<p_{\text{T}}^\text{max}<300$~GeV
YMin=0.025
# END PLOT
# BEGIN PLOT /CMS_2019_I1719955/d11-x01-y01
Title=CMS, 13 TeV, $\Delta\phi_{12}$ Z+2jet,$300<p_{\text{T}}^\text{max}<400$~GeV
YMin=0.029
# END PLOT
# BEGIN PLOT /CMS_2019_I1719955/d12-x01-y01
Title=CMS, 13 TeV, $\Delta\phi_{12}$ Z+2jet,$400<p_{\text{T}}^\text{max}<500$~GeV
YMin=0.029
# END PLOT
# BEGIN PLOT /CMS_2019_I1719955/d13-x01-y01
Title=CMS, 13 TeV, $\Delta\phi_{12}$ Z+2jet,$500<p_{\text{T}}^\text{max}<600$~GeV
YMin=0.029
# END PLOT
# BEGIN PLOT /CMS_2019_I1719955/d14-x01-y01
Title=CMS, 13 TeV, $\Delta\phi_{12}$ Z+2jet,$600<p_{\text{T}}^\text{max}<700$~GeV
YMin=0.021
# END PLOT
# BEGIN PLOT /CMS_2019_I1719955/d15-x01-y01
Title=CMS, 13 TeV, $\Delta\phi_{12}$ Z+2jet,$700<p_{\text{T}}^\text{max}<800$~GeV
YMin=0.021
# END PLOT
# BEGIN PLOT /CMS_2019_I1719955/d16-x01-y01
Title=CMS, 13 TeV, $\Delta\phi_{12}$ Z+2jet,$800<p_{\text{T}}^\text{max}<1000$~GeV
YMin=0.011
# END PLOT
# BEGIN PLOT /CMS_2019_I1719955/d17-x01-y01
Title=CMS, 13 TeV, $\Delta\phi_{12}$ Z+2jet,$1000<p_{\text{T}}^\text{max}<1200$~GeV
YMin=0.011
# END PLOT
# BEGIN PLOT /CMS_2019_I1719955/d18-x01-y01
Title=CMS, 13 TeV, $\Delta\phi_{12}$ Z+2jet,$p_{\text{T}}^\text{max}>1200$~GeV
YMin=0.011
# END PLOT
